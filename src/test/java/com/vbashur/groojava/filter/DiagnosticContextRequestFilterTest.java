package com.vbashur.groojava.filter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

class DiagnosticContextRequestFilterTest {

    private DiagnosticContextRequestFilter filter;

    @BeforeEach
    public void setUp() {
        filter = new DiagnosticContextRequestFilter(DiagnosticContextFilterConfiguration.DEFAULT_REQUEST_TOKEN_HEADER,
                DiagnosticContextFilterConfiguration.DEFAULT_MDC_UUID_TOKEN_KEY);
    }

    @Test
    @DisplayName("Given no Request-Id header incoming, When doFilter, Then populates Request-Id in response header")
    void shouldPopulateNewRequestIdInResponseHeader() throws Exception {
        HttpServletRequest request = new MockHttpServletRequest();
        HttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = new MockFilterChain();

        filter.doFilterInternal(request, response, filterChain);

        assertThat(filter.getRequestToken(), notNullValue());
        assertThat(response.getHeader(DiagnosticContextFilterConfiguration.DEFAULT_REQUEST_TOKEN_HEADER), notNullValue());
    }


    @Test
    @DisplayName("Given Request-Id header incoming, When doFilter, Then returns response header with same Request-Id")
    void shouldKeepOriginalRequestIdAtResponseHeader() throws Exception {
        String requestId = UUID.randomUUID().toString();
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader(DiagnosticContextFilterConfiguration.DEFAULT_REQUEST_TOKEN_HEADER, requestId);
        HttpServletResponse response = new MockHttpServletResponse();
        FilterChain filterChain = new MockFilterChain();

        filter.doFilterInternal(request, response, filterChain);

        assertThat(filter.getRequestToken(), is(requestId));
        assertThat(response.getHeader(DiagnosticContextFilterConfiguration.DEFAULT_REQUEST_TOKEN_HEADER), is(requestId));
    }

}