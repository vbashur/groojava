package com.vbashur.groojava.service;

import com.vbashur.groojava.domain.CodeProcessOutput;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyString;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;

@SpringBootTest
class SourceCodeExecutionServiceTest {

    @Autowired
    private SourceCodeExecutionService executionService;

    @MockBean
    private GroovyCodeProcess codeProcess;

    @Test
    @DisplayName("Given valid input source code, When 'run', Then returns execution output")
    public void shouldReturnExecutionOutputOnValidInput() throws IOException {

        String cmdOutput = UUID.randomUUID().toString();
        Process mockProcess = Mockito.mock(Process.class);
        Mockito.when(mockProcess.getInputStream()).thenReturn(new ByteArrayInputStream(cmdOutput.getBytes()));
        Mockito.when(mockProcess.getErrorStream()).thenReturn(new ByteArrayInputStream(new byte[0]));
        Mockito.when(codeProcess.get(anyString())).thenReturn(mockProcess);


        CodeProcessOutput output = executionService.run(new ByteArrayInputStream("whatever".getBytes()), "requestId");
        assertThat(output.output(), is(cmdOutput));
        assertThat(output.error(), is(emptyString()));
    }

    @Test
    @DisplayName("Given invalid input source code, When 'run', Then returns execution error")
    public void shouldReturnExecutionErrorOnInvalidInput() throws IOException {

        String errOutput = UUID.randomUUID().toString();
        Process mockProcess = Mockito.mock(Process.class);
        Mockito.when(mockProcess.getInputStream()).thenReturn(new ByteArrayInputStream(new byte[0]));
        Mockito.when(mockProcess.getErrorStream()).thenReturn(new ByteArrayInputStream(errOutput.getBytes()));
        Mockito.when(codeProcess.get(anyString())).thenReturn(mockProcess);

        CodeProcessOutput output = executionService.run(new ByteArrayInputStream("whatever".getBytes()), "requestId");
        assertThat(output.output(), is(emptyString()));
        assertThat(output.error(), is(errOutput));
    }


    @Test
    @DisplayName("Given service fails with I/O error on code execution, When 'run', Then returns error message")
    public void shouldReturnErrorMessageOnCodeExecutionFailure() throws IOException {
        String processorName = "test";
        Mockito.when(codeProcess.get(anyString())).thenThrow(IOException.class);
        Mockito.when(codeProcess.getName()).thenReturn(processorName);

        CodeProcessOutput output = executionService.run(new ByteArrayInputStream("whatever".getBytes()), "requestId");
        assertThat(output.error(), is("unable to run " + processorName + " code, error: null"));
    }


}