package com.vbashur.groojava.rest;

import com.vbashur.groojava.domain.ImmutableCodeProcessOutput;
import com.vbashur.groojava.service.SourceCodeExecutionService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

import static org.hamcrest.Matchers.startsWith;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class SubmitFileControllerTest {

    private static final String SUBMIT_URL = "/v1/submit";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SourceCodeExecutionService executionService;

    @Test
    @DisplayName("Given valid input data, When submit, Then returns success response")
    void shouldReturnSuccessResponse() throws Exception {
        MockMultipartFile mockInputFile = new MockMultipartFile("filedata", UUID.randomUUID().toString().getBytes());
        Mockito.when(executionService.run(any(ByteArrayInputStream.class), anyString()))
                .thenReturn(ImmutableCodeProcessOutput.builder()
                        .output("someoutput").build());

        mockMvc.perform(MockMvcRequestBuilders.multipart(SUBMIT_URL).file(mockInputFile))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"output\":\"someoutput\",\"error\":null}"));
    }

    @Test
    @DisplayName("Given IO error raised on submit, When submit, Then returns error response")
    void shouldReturnErrorResponse() throws Exception {
        MockMultipartFile mockInputFile = new MockMultipartFile("filedata", UUID.randomUUID().toString().getBytes());
        Mockito.when(executionService.run(any(ByteArrayInputStream.class), anyString()))
                .thenThrow(IOException.class);

        mockMvc.perform(MockMvcRequestBuilders.multipart(SUBMIT_URL).file(mockInputFile))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("error", startsWith("Please contact administrator.")));
    }

}