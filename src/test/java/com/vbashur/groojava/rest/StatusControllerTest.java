package com.vbashur.groojava.rest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {"spring.application.version=test-version"})
class StatusControllerTest {


    private static final String STATUS_URL = "/v1/status";

    @Autowired
    private MockMvc mockMvc;

    @Test
    @DisplayName("Given application version, When status request, Then returns expected version and healthy status response")
    void shouldReturnVersionAndStatus() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(STATUS_URL))
                .andExpect(status().isOk())
                .andExpect(jsonPath("version", is("test-version")))
                .andExpect(jsonPath("status", is("healthy")));

    }
}