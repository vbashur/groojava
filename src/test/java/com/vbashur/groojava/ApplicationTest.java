package com.vbashur.groojava;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anything;


@SpringBootTest
public class ApplicationTest {

    @Test
    public void applicationStarts() {
        assertThat("application should start successfully", true, anything());
    }
}
