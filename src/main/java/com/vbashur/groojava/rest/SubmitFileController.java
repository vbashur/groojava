package com.vbashur.groojava.rest;

import com.vbashur.groojava.domain.CodeProcessOutput;
import com.vbashur.groojava.domain.ImmutableCodeProcessOutput;
import com.vbashur.groojava.filter.DiagnosticContextRequestFilter;
import com.vbashur.groojava.service.SourceCodeExecutionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@Tag(name = "submit", description = "Code submission entrypoint")
public class SubmitFileController {

    private static final Logger LOG = LoggerFactory.getLogger(SubmitFileController.class);

    private final SourceCodeExecutionService executionService;

    private final DiagnosticContextRequestFilter requestFilter;

    @Autowired
    public SubmitFileController(SourceCodeExecutionService executionService,
                                DiagnosticContextRequestFilter requestFilter) {
        this.executionService = executionService;
        this.requestFilter = requestFilter;
    }

    @Operation(summary = "Submits the file with the source code and produces execution output")
    @PostMapping(value = {"/v1/submit"}, produces = "application/json")
    public @ResponseBody
    ResponseEntity<CodeProcessOutput> submit(
            @RequestParam MultipartFile filedata) throws IOException {
        CodeProcessOutput output = executionService.run(filedata.getInputStream(), requestFilter.getRequestToken());
        return new ResponseEntity<>(output, HttpStatus.OK);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<CodeProcessOutput> handleInternalIOErrors(IOException ex) {
        LOG.error("request failed: " + ex.getMessage());
        return new ResponseEntity<>(ImmutableCodeProcessOutput.builder()
                .error("Please contact administrator. Unable to execute the request " + requestFilter.getRequestToken())
                .build(),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
