package com.vbashur.groojava.rest;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@Tag(name = "status", description = "Application status")
public class StatusController {

    private final String version;

    public StatusController(@Value("${spring.application.version}") String version) {
        this.version = version;
    }

    @GetMapping(value = {"/v1/status"}, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> status() {
        JSONObject resultBody = new JSONObject()
                .put("version", version)
                .put("status", "healthy"); // expected technical debt
        return ResponseEntity
                .status(OK)
                .body(resultBody.toString());
    }

}
