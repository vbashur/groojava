package com.vbashur.groojava.service;

import java.io.IOException;

public interface CodeProcess {

    Process get(String pathToFile) throws IOException;

    String getName();
}
