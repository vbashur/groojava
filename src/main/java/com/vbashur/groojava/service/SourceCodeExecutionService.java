package com.vbashur.groojava.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vbashur.groojava.domain.CodeProcessOutput;
import com.vbashur.groojava.domain.ImmutableCodeProcessOutput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;

@Service
public class SourceCodeExecutionService {

    private static final Logger LOG = LoggerFactory.getLogger(SourceCodeExecutionService.class);

    private final CodeProcess codeProcess;

    @Autowired
    public SourceCodeExecutionService(CodeProcess codeProcess) {
        this.codeProcess = codeProcess;
    }

    public CodeProcessOutput run(InputStream sourceCode, String requestId) throws IOException {

        Path tmpFile = null;
        try {
            LOG.trace("executing source code using '" + codeProcess.getName() + "' code processor");
            tmpFile = Files.createTempFile(requestId + "-", "-groojava.groovy");
            Files.write(tmpFile, sourceCode.readAllBytes());

            Process process = codeProcess.get(tmpFile.toAbsolutePath().toString());

            ImmutableCodeProcessOutput.Builder resBuilder = ImmutableCodeProcessOutput.builder();
            resBuilder.output(getProcessResult(process.getInputStream()));
            resBuilder.error(getProcessResult(process.getErrorStream()));
            CodeProcessOutput processOutput = resBuilder.build();
            LOG.trace("code execution result: " + new ObjectMapper().writeValueAsString(processOutput));
            return processOutput;
        } catch (IOException e) {
            String errorMessage = String.format("unable to run %s code, error: %s", codeProcess.getName(), e.getMessage());
            LOG.error(errorMessage);
            return ImmutableCodeProcessOutput.builder()
                    .error(errorMessage)
                    .build();
        } finally {
            if (tmpFile != null) {
                Files.delete(tmpFile);
            }
        }
    }

    private String getProcessResult(InputStream processInputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(processInputStream))) {
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line);
            }
        }
        return output.toString();
    }
}

