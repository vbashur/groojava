package com.vbashur.groojava.service;

import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class GroovyCodeProcess implements CodeProcess {

    private static final String GROOVY_CMD_NAME = "groovy";

    @Override
    public Process get(String pathToFile) throws IOException {
        String command = GROOVY_CMD_NAME + " " + pathToFile;
        return Runtime.getRuntime().exec(command);
    }

    @Override
    public String getName() {
        return GROOVY_CMD_NAME;
    }

}
