package com.vbashur.groojava.domain;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;
import org.springframework.lang.Nullable;

@Value.Immutable
@JsonSerialize(as = ImmutableCodeProcessOutput.class)
@JsonDeserialize(as = ImmutableCodeProcessOutput.class)
public interface CodeProcessOutput {

    @Value.Parameter
    @Nullable
    String output();

    @Value.Parameter
    @Nullable
    String error();

}
