package com.vbashur.groojava.filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DiagnosticContextFilterConfiguration {

    public static final String DEFAULT_REQUEST_TOKEN_HEADER = "Request-ID";
    public static final String DEFAULT_MDC_UUID_TOKEN_KEY = "DiagnosticContextFilter.RequestId";

    @Bean
    @SuppressWarnings("unchecked")
    public FilterRegistrationBean servletRegistrationBean(DiagnosticContextRequestFilter diagnosticContextRequestFilter) {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(diagnosticContextRequestFilter);
        registrationBean.setOrder(2);
        return registrationBean;
    }

    @Bean
    public DiagnosticContextRequestFilter diagnosticContextRequestFilter() {
        return new DiagnosticContextRequestFilter(
                DEFAULT_REQUEST_TOKEN_HEADER,
                DEFAULT_MDC_UUID_TOKEN_KEY);
    }
}

