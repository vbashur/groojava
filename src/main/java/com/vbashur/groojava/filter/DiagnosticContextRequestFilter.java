package com.vbashur.groojava.filter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

public class DiagnosticContextRequestFilter extends OncePerRequestFilter {

    private final String mdcTokenKey;
    private final String requestHeader;
    private String token;


    public DiagnosticContextRequestFilter(String requestHeader, String mdcTokenKey) {
        super();
        this.requestHeader = requestHeader;
        this.mdcTokenKey = mdcTokenKey;
    }

    /**
     * @return request token that was handled or generated (if none provided externally) by filter
     */
    public String getRequestToken() {
        return this.token;
    }

    /**
     * Reads incoming request and seeks for a header value mapped to 'Request-ID' key then store its value as 'token'
     * If none is there - generates random uuid token, eg 6623e29f-8408-47dd-9104-8af99d822ec7
     * <p>
     * Sets the 'Request-ID' response header with 'token' value
     *
     * @param request
     * @param response
     * @param chain
     * @throws java.io.IOException
     * @throws ServletException
     */
    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain)
            throws java.io.IOException, ServletException {
        try {
            if (StringUtils.isEmpty(requestHeader) || StringUtils.isEmpty(request.getHeader(requestHeader))) {
                token = UUID.randomUUID().toString();
            } else {
                token = request.getHeader(requestHeader);
            }
            MDC.put(mdcTokenKey, token);
            response.addHeader(requestHeader, token);
            chain.doFilter(request, response);
        } finally {
            MDC.remove(mdcTokenKey);
        }
    }
}
