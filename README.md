# Groojava

## Overview

Groojava is a web service (written in java) that allows users to submit Groovy code
(https://groovy-lang.org/) for execution and get back results of the computation.

## Setup
Run `git clone` to get the source code from repository

```
$ git clone <web_proj_url>
```

To run and use the project you need to have *docker* engine and *docker-compose* util. Refer to **References** section to get the installation manuals.


## Local Service Execution

The service is running inside a docker container; to start it, execute (within the project folder):

```
$ docker-compose up --build
```

The service exposes port 8080, refer to the docker-compose.yml file to change the port mapping

Once the application is running, check the status of the application using the following URL: http://localhost:8080/v1/status


## Submit groovy code

Use a file with valid groovy code and submit it with HTTP POST request to the following endpoint `http://localhost:8080/v1/submit`

Groovy file content sample (file `/tmp/code.groovy`)
```groovy
class SampleGroovy {

    static void main(def args){
        def mylist= [1,2,"Hey","4"]
        mylist.each{ println it }
    }
}
```
POST request sample
```
$ curl  -X POST  -i  -F "filedata=@/tmp/code.groovy" http://localhost:8080/v1/submit
```


## API Documentation

Refer to the [Swagger API Documentation](http://localhost:8080/swagger-ui.html) for details about API exposed.


## Testing

```
$ cd groojava
$ ./run-tests.sh
```

## References

Following links guide you through installation setup for prerequisite software

* [Docker engine](https://docs.docker.com/engine/install)
* [Docker compose](https://docs.docker.com/compose/install/)
* [Java Development Kit](https://www.oracle.com/java/technologies/javase-downloads.html)
* [Groovy](https://groovy-lang.org/)
