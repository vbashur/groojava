#!/usr/bin/env bash

# shell script required to run the app
exec java -jar /app/groojava-*.jar "$@"
